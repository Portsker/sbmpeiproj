from .models import myTask
from django import forms
class taskForm(forms.ModelForm):
    class Meta:
        model = myTask
        fields = ('name',
                  'date_of_end',
        )
        widgets = {
            'date_of_end': forms.DateInput(attrs={'class':'datepicker'}),
        }
