from django.shortcuts import render
from django.shortcuts import render_to_response,HttpResponse,redirect,render
from .models import myTask
from django.http import JsonResponse, HttpResponse, Http404
from django.utils import timezone
import datetime
from .forms import taskForm
# Create your views here.

def entryPoint(request): # список всех мероприятий
    return render(request,'tasks.html')

def showTasks(request): # список всех мероприятий
    allTasks = list(myTask.objects.all().values()) #status: 0 - actual, 1 - done,  2 - overdue
    now=datetime.date.today()
    myTask.objects.filter(date_of_end__lte=now ).exclude(status=1).update(status=2)
    myTask.objects.filter(date_of_end__gte=now ).exclude(status=1).update(status=0)
    return JsonResponse(allTasks, safe=False)

def edit(request, task_id):
    try:
        myinstance = myTask.objects.get(pk=task_id)
    except:
        raise Http404("не создали :(")
    if request.POST:
        form = taskForm(request.POST, instance=myinstance)
        print(myinstance)
        form.save()
        allTasks = list(myTask.objects.all().values())
        return redirect('/tasks/all/')
    else:
        form = taskForm(instance=myinstance)
        return render(request, 'edit.html', {'form': form, 'id':task_id})

def delete(request, task_id): #TODO Done button
    p = myTask.objects.get(pk=task_id)
    print(p)
    try:
        print(task_id)
        p = myTask.objects.get(pk=task_id)
        print(p)
        p.delete()
        allTasks = list(myTask.objects.all().values())
        return JsonResponse(allTasks, safe=False)
    except:
        raise Http404("Такого юзера еще не создали :(")

def done(request, task_id):
    p = myTask.objects.get(pk=task_id)
    p.status = 1
    p.save()
    allTasks = list(myTask.objects.all().values())
    return JsonResponse(allTasks, safe=False)

def add(request):
    if request.POST:
        form = taskForm(request.POST)
        if form.is_valid():
            form.save()
        else:
            print('Form is not valid')
        return redirect('/tasks/all/')
    else:
        form = taskForm()
        return render(request, 'add.html', {'form': form})
