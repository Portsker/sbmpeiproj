from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^showTasks/$', views.showTasks, name='showusers'),
    url(r'^edit/(?P<task_id>\d+)/$', views.edit),
    url(r'^delete/(?P<task_id>\d+)/$', views.delete),
    url(r'^add/$', views.add),
    url(r'^all/$', views.entryPoint),
    url(r'^done/(?P<task_id>\d+)/$', views.done),
]
