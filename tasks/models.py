from django.db import models
from django.utils import timezone
import datetime
# Create your models here.
class myTask(models.Model):
    name = models.TextField(default = '',max_length=100)
    #option = models.ForeignKey(option,default=1,on_delete=models.CASCADE)
    date_of_end = models.DateField(default=timezone.now())
    status = models.IntegerField(default=0)
    def __unicode__(self):
        return self.name
    def __str__(self):
        return self.name
